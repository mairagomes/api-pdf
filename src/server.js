const express = require ('express')
const ejs = require('ejs')
const path = require('path')
const pdf = require('html-pdf')
const app = express()
const fs = require('fs')


let passengers = {}
fs.readFile('passengers.json', 'utf-8', (err, data) => {
  if (err) throw err
  passengers = JSON.parse(data)
})

app.get('/', (request, response) => {

    const filePath =  path.join(__dirname,"../views/print.ejs")
    ejs.renderFile(filePath, {passengers}, (err, html) => {
        if(err) {
            return response.send('Erro na leitura do arquivo')
        } 
    // criacao do pdf 
    pdf.create(html).toFile("report.pdf", (err, data) => {
        if(err) {
            return response.send("Erro ao gerar o pdf")
        }
    })
        //envia para o navegador
        return response.send(html)
    })

})
app.use(express.static('public'))
app.listen(3000)